import java.util.Scanner;
import java.util.Vector;
import java.util.List;

// Smell tidak express intent case1 itu apa? ga menjelaskan gambaran umum dari program ini
// solusi ganti nama kelasnya yang lebih ekspresif
public class Case1{
    public static void main(String[] args){
        new Program().begin();
    }
}

// GOD Class(Large Class) kelas ini terlalu banyak melakukan tindakan dari membaca input, menentukan input itu kemana, dan mengolah proses dari inputan di kelas ini
// solusi extract class
class Program{
    // Long method dia lakuin apapun disini selain methodnya panjang juga violate di express intent
    // solusi extract methodnya
    public void begin(){
        Scanner input = new Scanner(System.in);

        int choice = -1;

        //MAIN MENU
        Transaction transaction = new Transaction();
        do{
            System.out.println("=======================");
            System.out.println("=== Shopping Lists ===");
            System.out.println("=======================");
            System.out.println("Cart: ");
            for(int i = 1; i <= transaction.getAllName().size(); i++){
                String transName = transaction.getName(i);
                int transPrice = transaction.getPrice(i);
                int transQuantity = transaction.getQuantity(i);
                System.out.println(i + ". " + transName + "(" + transQuantity + ") = $" + transPrice * transQuantity);
            }
            System.out.println("=======================");
            System.out.println("1. Add Item");
            System.out.println("2. Remove Item");
            System.out.println("3. Checkout");
            System.out.println("0. Exit");
            System.out.println("=======================");
            do{
                choice = -1;
                try{
                    System.out.print("=> ");
                    String inputText = input.nextLine();
                    choice = Integer.parseInt(inputText);
                }
                catch(Exception e){}
            } while (choice < 0 || choice > 3);

            // Smell switch case statement, ketika ada fitur baru atau spec baru maka line switch case ini bakalan membengkak yang mana jadi code smell
            // solusi switch case, move method setiap case, atau menggunakan command pattern
            switch(choice){
                //Add Item to cart
                case 1:
                    System.out.print("\n\n\n\n\n");
                    System.out.println("================");
                    System.out.println("=== Add Item ===");
                    System.out.println("================");
                    System.out.print("Item Name: ");
                    String name = input.nextLine();
                    System.out.print("Item Price: ");
                    int price;
                    do{
                        price = -1;
                        try{
                            System.out.print("=> ");
                            String inputTextPrice = input.nextLine();
                            price = Integer.parseInt(inputTextPrice);
                        }
                        catch(Exception e){}
                    } while (price <= 0);
                    System.out.print("Item Quantity: ");
                    int quantity;
                    do{
                        quantity = -1;
                        try{
                            System.out.print("=> ");
                            String inputTextQuantity = input.nextLine();
                            quantity = Integer.parseInt(inputTextQuantity);
                        }
                        catch(Exception e){}
                    } while (quantity <= 0);

                    transaction.addItem(name, price, quantity);

                    System.out.print("\n\n\n\n\n");
                    break;
                //Remove Item from cart
                case 2:
                    System.out.print("\n");
                    if(transaction.getAllName().size() > 0){
                        int target;
                        do{
                            target = -1;
                            try{
                                System.out.print("Item to Remove [1 - " + transaction.getAllName().size() + "]: ");
                                String inputTextTarget = input.nextLine();
                                target = Integer.parseInt(inputTextTarget);
                            }
                            catch(Exception e){}
                        } while (target < 1 || target > transaction.getAllName().size());

                        System.out.println("Delete success");
                        transaction.remove(Transaction.SINGLE, target);
                    }
                    else{
                        System.out.println("Cart is empty");
                    }

                    input.nextLine();
                    System.out.print("\n\n\n\n\n");
                    break;
                //Buy Items
                case 3:
                    if(transaction.getAllName().size() > 0){
                        int total = 0;
                        for(int i = 1; i <= transaction.getAllName().size(); i++){
                            int transPrice = transaction.getPrice(i);
                            int transQuantity = transaction.getQuantity(i);
                            total += transPrice * transQuantity;
                        }
                        System.out.println("Total purchase: $" + total);
                        transaction.remove(Transaction.ALL, 0);
                    }
                    else{
                        System.out.println("Cart is empty");
                    }

                    input.nextLine();
                    System.out.print("\n\n\n\n\n");
                    break;
            }
        } while (choice != 0);
    } 
}

// Class ini sudah bagus tapi ada beberapa behaviour yang bisa di extract lagi contohnya adalah item, kita dapat mengekstrak item ke kelas sendiri
// solusi extract class
class Transaction{
    public static final int SINGLE = 0;
    public static final int ALL = 1;  

    // bagian ini violate Primitive Obsession sebenernya attribut attribut ini adalah bagian dari item, kita dapat mengekstraknya ke kelas item
    protected List<Integer> quantity;
    protected List<Integer> price;
    protected List<String> name;

    public Transaction(){
        this.quantity = new Vector<Integer>();
        this.price = new Vector<Integer>();
        this.name = new Vector<String>();
    }

    public void addItem(String name, int price, int quantity){
        this.quantity.add(quantity);
        this.price.add(price);
        this.name.add(name);
    }
    // smell penamaan variabel apa itu i? better index
    public void remove(int type, int i){
        if(type == SINGLE){
            this.quantity.remove(i - 1);
            this.price.remove(i - 1);
            this.name.remove(i - 1);
        }
        else if(type == ALL){
            this.quantity.clear();
            this.price.clear();
            this.name.clear();
        }
    }

    public int getQuantity(int i){
        return this.quantity.get(i - 1);
    }
    // Unused code tidak ada yang memanggil function ini
    public List<Integer> getAllQuantity(){
        return this.quantity;
    }

    public int getPrice(int i){
        return this.price.get(i - 1);
    }
    public List<Integer> getAllPrice(){
        return this.price;
    }

    public String getName(int i){
        return this.name.get(i - 1);
    }
    public List<String> getAllName(){
        return this.name;
    }
}