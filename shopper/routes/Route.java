package shopper.routes;

import shopper.controllers.AddItemController;
import shopper.controllers.CheckoutController;
import shopper.controllers.ExitController;
import shopper.controllers.RemoveItemController;
import shopper.invoker.Invoker;
import shopper.service.TransactionService;

public class Route {
  private Invoker invoker;
  private TransactionService transactionService;

  public Route(Invoker invoker, TransactionService transactionService){
    this.invoker = invoker;
    this.transactionService = transactionService;
  }

  public void register(){
    AddItemController addItem = new AddItemController(this.transactionService);
    RemoveItemController removeItem = new RemoveItemController(this.transactionService);
    CheckoutController checkoutController = new CheckoutController(this.transactionService);
    ExitController exit = new ExitController();

    this.invoker.register(1, addItem);
    this.invoker.register(2, removeItem);
    this.invoker.register(3, checkoutController);
    this.invoker.register(0, exit);
  }
}
