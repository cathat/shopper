package shopper;
import shopper.controllers.MenuController;
import shopper.invoker.Invoker;
import shopper.routes.Route;
import shopper.service.TransactionService;

public class Shopper {
  public static void main(String[] args){
    TransactionService transactionService = new TransactionService();
    Invoker invoker = new Invoker();

    Route route = new Route(invoker, transactionService);
    route.register();
    MenuController menu = new MenuController(transactionService);
    while(true){
      menu.run(invoker);
    }
  }
}
