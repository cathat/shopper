package shopper.controllers;

import java.util.List;

import shopper.models.Item;
import shopper.service.TransactionService;
import shopper.view.CheckoutMenu;

public class CheckoutController implements Command {
  private TransactionService transactionService;

  public CheckoutController(TransactionService transactionService){
    this.transactionService = transactionService;
  }

  public List<Item> getAllItem() {
    return transactionService.getAllItem();
  }

  public boolean isChartEmpty(){
    return this.transactionService.isChartEmpty();
  }

  public void checkout(){
    this.transactionService.removeAll();
  }

  @Override
  public void execute() {
    CheckoutMenu menu = new CheckoutMenu(this);
    menu.display();
  }
}
