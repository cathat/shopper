package shopper.controllers;

public interface Command {
  void execute();
}
