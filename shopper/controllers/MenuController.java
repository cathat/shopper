package shopper.controllers;
import shopper.invoker.Invoker;
import shopper.models.Item;
import shopper.service.TransactionService;
import shopper.view.MainMenu;
import java.util.List;

public class MenuController {
  private TransactionService transactionService;

  public MenuController(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  public void run(Invoker invoker) {
    MainMenu menu = new MainMenu(invoker, this);
    menu.display();
  }

  public List<Item> getAllItem() {
    return transactionService.getAllItem();
  }
}
