package shopper.controllers;

import shopper.service.TransactionService;
import shopper.view.RemoveItemMenu;

public class RemoveItemController implements Command{
  private TransactionService transactionService;

  public RemoveItemController(TransactionService transactionService){
    this.transactionService = transactionService;
  }

  public boolean isChartEmpty(){
    return transactionService.isChartEmpty();
  }

  public int chartSize(){
    return transactionService.getAllItem().size();
  }

  public void removeOne(int target){
    transactionService.removeOne(target);
  }

  @Override
  public void execute() {
    RemoveItemMenu removeItemMenu = new RemoveItemMenu(this);
    removeItemMenu.display();
  }
}
