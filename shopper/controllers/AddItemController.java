package shopper.controllers;
import shopper.service.TransactionService;
import shopper.view.AddItemMenu;

public class AddItemController implements Command {
  private TransactionService transactionService;

  public AddItemController(TransactionService transactionService){
    this.transactionService = transactionService;
  }

  public void addItem(String name, int price, int quantity){
    transactionService.addItem(name, price, quantity);
  }

  @Override
  public void execute() {
    AddItemMenu itemMenu = new AddItemMenu();
    itemMenu.display(this);
  }
}
