package shopper.controllers;

public class ExitController implements Command {

  @Override
  public void execute() {
    System.exit(0);
  }
  
}
