package shopper.invoker;
import java.util.HashMap;
import shopper.controllers.Command;
import shopper.view.BaseView;

public class Invoker {
  private final HashMap<Integer, Command> commandMap = new HashMap<>();
    
  public void register(Integer commandName, Command command) {
      commandMap.put(commandName, command);
  }
  
  public void execute(Integer commandName) {
      Command command = commandMap.get(commandName);
      if (command == null) {
         BaseView base = new BaseView();
         base.display(this);
         return;
      }
      command.execute();
  }
}
