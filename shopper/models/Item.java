package shopper.models;

public class Item {
  private int quantity;
  private int price;
  private String name;

  public Item(int quantity, int price, String name){
    this.quantity = quantity;
    this.price = price;
    this.name = name;
  }

  public int getQuantity(){
    return this.quantity;
  }

  public int getPrice(){
    return this.price;
  }

  public String getName(){
    return this.name;
  }
}
