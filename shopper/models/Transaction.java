package shopper.models;

import java.util.List;
import java.util.Vector;

public class Transaction {
  private List<Item> items;

  public Transaction(){
    this.items = new Vector<Item>();
  }

  public void addItem(String name, int price, int quantity){
    Item item = new Item(quantity, price, name);
    this.items.add(item);
  }

  public void remove(int index){
    this.items.remove(index - 1);
  }

  public void removeAll(){
    this.items.clear();
  }

  public List<Item> getAllItem(){
    return this.items;
  }
}
