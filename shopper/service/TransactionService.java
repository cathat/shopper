package shopper.service;
import shopper.models.Item;
import shopper.models.Transaction;
import java.util.List;

public class TransactionService {
  private Transaction transaction;

  public TransactionService(){
    this.transaction = new Transaction();
  }

  public void addItem(String name, int price, int quantity){
    transaction.addItem(name, price, quantity);
  }

  public List<Item> getAllItem(){
    return transaction.getAllItem();
  }

  public boolean isChartEmpty(){
    if(transaction.getAllItem().size() == 0){
      return true;
    }

    return false;
  }

  public void removeOne(int target){
    transaction.remove(target);
  }

  public void removeAll(){
    transaction.removeAll();
  }
}
