package shopper.view;

import shopper.controllers.AddItemController;

public class AddItemMenu extends BaseView {

  public void display(AddItemController controller) {
    System.out.print("\n\n\n\n\n");
    System.out.println("================");
    System.out.println("=== Add Item ===");
    System.out.println("================");

    System.out.print("Item Name: ");
    String name = this.input.nextLine();
    System.out.print("Item Price: ");
    int price = getPrice();
    System.out.print("Item Quantity: ");
    int quantity = getQuantity();

    controller.addItem(name, price, quantity);
  }

  private Integer getPrice(){
    int price;
    do{
      price = -1;
      try{
          System.out.print("=> ");
          String inputTextPrice = input.nextLine();
          price = Integer.parseInt(inputTextPrice);
      }
      catch(Exception e){}
    } while (price <= 0);

    return price;
  }

  private Integer getQuantity(){
    int quantity;
    do{
        quantity = -1;
        try{
            System.out.print("=> ");
            String inputTextQuantity = input.nextLine();
            quantity = Integer.parseInt(inputTextQuantity);
        }
        catch(Exception e){}
    } while (quantity <= 0);

    return quantity;
  }
}
