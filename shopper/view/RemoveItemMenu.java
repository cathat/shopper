package shopper.view;

import shopper.controllers.RemoveItemController;

public class RemoveItemMenu extends BaseView {
  private RemoveItemController controller;

  public RemoveItemMenu(RemoveItemController controller){
    this.controller = controller;
  }

  public void display(){
    System.out.print("\n\n\n\n\n");
    
    if(controller.isChartEmpty()){
      System.out.println("Empty");
    }else{
      remove();
    }
  }

  public void remove(){
    int target;
    do{
      target = -1;
      try {
        System.out.print("Item to Remove [1 - " + controller.chartSize() + "]: ");
        String inputTextTarget = this.input.nextLine();
        target = Integer.parseInt(inputTextTarget);
      } catch (Exception e) {}
    }while (target < 1 || target > controller.chartSize());
    
    System.out.println("Delete success");
    controller.removeOne(target);
  }
}
