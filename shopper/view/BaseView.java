package shopper.view;

import java.util.Scanner;

import shopper.invoker.Invoker;

public class BaseView {
  protected Scanner input;

  public BaseView(){
    Scanner input = new Scanner(System.in);
    this.input = input;
  }

  public void display(Invoker invoker){
    System.out.println("Command not registered, pls try again with different command!");

    System.out.print("=> ");
    final String inputText = this.input.nextLine();
    final int command = Integer.parseInt(inputText);

    invoker.execute(command);
  }
}
