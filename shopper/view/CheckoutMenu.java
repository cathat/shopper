package shopper.view;

import java.util.List;

import shopper.controllers.CheckoutController;
import shopper.models.Item;

public class CheckoutMenu extends BaseView {
  private CheckoutController controller;

  public CheckoutMenu(CheckoutController controller){
    this.controller = controller;
  }

  public void display(){
    System.out.print("\n\n\n\n\n");
    if(controller.isChartEmpty()){
      System.out.println("Cart is empty");
    }else{
      checkout();
    }

    this.input.nextLine();
  }

  private void checkout(){
    int total = 0;
    List<Item> items = controller.getAllItem();
    for(int i = 1; i <= items.size(); i++){
      int transPrice = items.get(i - 1).getPrice();
      int transQuantity = items.get(i - 1).getQuantity();
      total += transPrice * transQuantity;
    }
    System.out.println("Total purchase: $" + total);
    controller.checkout();
  }
}
