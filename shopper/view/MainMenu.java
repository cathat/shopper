package shopper.view;
import shopper.controllers.MenuController;
import shopper.invoker.Invoker;
import shopper.models.Item;

import java.util.List;

public class MainMenu extends BaseView{
  private final Invoker invoker;
  private MenuController controller;

  public MainMenu(final Invoker invoker, MenuController controller) {
    this.invoker = invoker;
    this.controller = controller;
  }

  public void display() {
    System.out.println("=======================");
    System.out.println("=== Shopping Lists ===");
    System.out.println("=======================");
    chart();
    menu();
  }

  private void menu() {
    System.out.println("=======================");
    System.out.println("1. Add Item");
    System.out.println("2. Remove Item");
    System.out.println("3. Checkout");
    System.out.println("0. Exit");
    System.out.println("=======================");
    input();
  }

  private void input() {
    System.out.print("=> ");
    final String inputText = this.input.nextLine();
    final int command = Integer.parseInt(inputText);

    this.invoker.execute(command);
  }

  private void chart() {
    System.out.println("Cart: ");

    List<Item> items = controller.getAllItem();
    for(int i = 1; i <= items.size(); i++){
      String transName = items.get(i - 1).getName();
      int transPrice = items.get(i - 1).getPrice();
      int transQuantity = items.get(i - 1).getQuantity();
      System.out.println(i + ". " + transName + "(" + transQuantity + ") = $" + transPrice * transQuantity);
    }
  }
}
