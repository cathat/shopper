# Shopper

Application to shop via CLI add item, remove and checkout.
Case1.java is deprecated one, and refactor move to shopper folder

## Pre-requites
- Java (we used openJDK 13.0.1)
- make(build-essential)
    - if you're using Linux you can install with `sudo apt-get install build-essential`
    - for mac os `brew install make`
    - for windows an refer to this [install make](https://stat545.com/make-windows.html)

## How To
- To build this program you can run `make build`
- To run this program you can run `make run`

## Diagram

![shopper diagram](./shopper-diagram.png)
